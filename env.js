module.exports = {
  title: 'Harry Potter app',
  lang: 'es',
	prefix: '/',
	port: 5010,
  api: 'http://localhost:3000',
  localStoragePrefix: 'harry-potter-app-',
  mode: 'development',
  assetsPrefix: 'assets'
};
